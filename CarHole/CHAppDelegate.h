//
//  CHAppDelegate.h
//  CarHole
//
//  Created by Mike Glass on 7/21/13.
//  Copyright (c) 2013 Pocket Sevens. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CHAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
