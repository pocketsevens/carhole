//
//  CHFlipsideViewController.m
//  CarHole
//
//  Created by Mike Glass on 7/21/13.
//  Copyright (c) 2013 Pocket Sevens. All rights reserved.
//

#import "CHFlipsideViewController.h"

@interface CHFlipsideViewController ()

@end

@implementation CHFlipsideViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

- (IBAction)done:(id)sender
{
    [self.delegate flipsideViewControllerDidFinish:self];
}

@end
