//
//  CHMainViewController.h
//  CarHole
//
//  Created by Mike Glass on 7/21/13.
//  Copyright (c) 2013 Pocket Sevens. All rights reserved.
//

#import "CHFlipsideViewController.h"

@interface CHMainViewController : UIViewController <CHFlipsideViewControllerDelegate>

@end
