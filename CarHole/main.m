//
//  main.m
//  CarHole
//
//  Created by Mike Glass on 7/21/13.
//  Copyright (c) 2013 Pocket Sevens. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CHAppDelegate.h"

int main(int argc, char *argv[])
{
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([CHAppDelegate class]));
	}
}
