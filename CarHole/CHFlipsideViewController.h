//
//  CHFlipsideViewController.h
//  CarHole
//
//  Created by Mike Glass on 7/21/13.
//  Copyright (c) 2013 Pocket Sevens. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CHFlipsideViewController;

@protocol CHFlipsideViewControllerDelegate
- (void)flipsideViewControllerDidFinish:(CHFlipsideViewController *)controller;
@end

@interface CHFlipsideViewController : UIViewController

@property (weak, nonatomic) id <CHFlipsideViewControllerDelegate> delegate;

- (IBAction)done:(id)sender;

@end
